var template = [
    1,1,1,0,0,0,0,0,0,1,0,1,0,1,1,
    0,0,1,0,1,1,1,1,0,0,0,1,0,0,0,
    1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,
    1,1,0,1,1,0,1,0,1,0,1,0,1,0,0,
    0,0,0,1,0,0,1,0,0,0,1,0,1,0,1,
    0,1,0,1,1,0,0,1,0,1,1,0,0,0,1,
    0,1,0,1,0,1,0,1,0,0,0,0,1,0,1,
    1,1,0,0,0,0,0,0,1,1,1,1,1,0,1,
    0,0,0,1,1,0,1,0,0,0,0,0,0,0,1,
    0,1,1,0,1,0,1,1,1,1,1,1,0,1,1,
    0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,
    0,1,1,1,0,1,0,1,0,1,0,1,0,0,0,
    0,1,1,0,0,1,0,1,0,1,0,0,1,1,0,
    0,1,0,1,0,1,0,1,0,0,1,0,1,1,0,
    0,0,1,0,0,0,0,1,1,2,1,0,0,0,0];

var canvas = document.getElementById("canvas")
var ctx = canvas.getContext("2d");

var cat = new Image();
cat.src = "res/cat.png";

var tuna = new Image();
tuna.src = "res/tuna.png";

var wall = new Image();
wall.src = "res/wall.png";

var maze = [];

var player = new Object({x:0,y:1,loc: 15, moves: 0});

var x = 0, y = 0;
for (var i = 0;i < 15 * 15;i++){
	maze.push({"x":x,"y":y,"state":template[i]});
	if (x == 14){
		y++;
		x = 0;
	}
	else {x++;}
}

function keyPress(e){
	
	switch(e.keyCode){
		case 68: //Right, D
			if (player.x != 14){
				player.loc ++;
				if (maze[player.loc].state != 1){
					ctx.clearRect(player.x * 32,player.y * 32, 32, 32);
					player.x++;
					player.moves ++;
				}
				else {
					player.loc --;
				}
			}
			break;
		case 65: //Left, A
			if (player.x != 0){
				player.loc --;
				if (maze[player.loc].state != 1){
					ctx.clearRect(player.x * 32,player.y * 32, 32, 32);
					player.x --;
					player.moves ++;
				}
				else {
					player.loc ++;
				}
			}
			break;
		case 83: //Down, S
			if (player.y != 14){
				player.loc += 15;
				if (maze[player.loc].state != 1){
					ctx.clearRect(player.x * 32,player.y * 32, 32, 32);
					player.y ++;
					player.moves ++;
				}
				else {
					player.loc -= 15;
				}
			}
			break;
		case 87: //Up, W
			if (player.y != 0){
				player.loc -= 15;
				if (maze[player.loc].state != 1){
					ctx.clearRect(player.x * 32,player.y * 32, 32, 32);
					player.y --;
					player.moves ++;
				}
				else {
					player.loc += 15;
				}
			}
			break;
	}
	
	ctx.drawImage(cat,player.x * 32,player.y * 32,32,32);
	if (maze[player.loc].state == 2){
		alert("You win!");
		location.reload();
	}
	
	document.getElementById("moves").innerHTML = "Moves: " + player.moves;
}

function start(){
	for (var i = 0;i < 15 * 15;i++){
		if (maze[i].state == 1 || maze[i].state == "1")
			ctx.drawImage(wall,maze[i].x * 32, maze[i].y * 32, 30, 30);
		if (maze[i].state == 2 || maze[i].state == "2")
			ctx.drawImage(tuna,maze[i].x * 32, maze[i].y * 32, 30, 30);
	}
	ctx.drawImage(cat,player.x * 32,player.y * 32,32,32);
	
	window.addEventListener("keydown", keyPress, true);
}